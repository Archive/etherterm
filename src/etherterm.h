#ifndef _ETHERTERM_H_
#define _ETHERTERM_H_

#include <config.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>

#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gdk/gdkprivate.h>
#include <gnome.h>
#include <zvt/zvtterm.h>
#include <libgnomeui/gnome-window-icon.h>

#include <X11/Xatom.h>
#include <gtk/gtkwidget.h>

typedef struct _EtherWindow EtherWindow;

#include "menu.h"
#include "prefs.h"
#include "term.h"

struct _EtherWindow {
  GtkWidget * window;
  GtkWidget * notebook;
  gint num_pages;
  gchar ** environ;
  EtherPrefs * eprefs;
  EtherWidgets * widgets;
};

#endif
