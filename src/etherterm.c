#include "etherterm.h"

static EtherPrefs * et_prepare_app (void) {
  EtherPrefs * prefs;
  gchar * font = "Options/Font=-schumacher-clean-medium-r-normal-*-*-80-*-*-c-*-*";
  gchar * wordclass = "Options/WordClass=-A-Za-z0-9,./?%&#_";
  gchar * scheme[4] = { "linux", "xterm", "rxvt", "custom" };
  gchar * scpos[3] = { "left", "right", "hidden" };
  gchar * temp;
  gint i;

  prefs = g_new0 (EtherPrefs, 1);

  gnome_config_push_prefix ("EtherTerm/");

  prefs->font = gnome_config_get_string (font);
  prefs->wordclass = gnome_config_get_string (wordclass);

  prefs->use_bold = gnome_config_get_bool ("Options/Bold=FALSE");
  prefs->blink = gnome_config_get_bool ("Options/Blink=TRUE");
  prefs->login = gnome_config_get_bool ("Options/Login=TRUE");
  prefs->menu_hide = gnome_config_get_bool ("Options/HideMenu=FALSE");
  prefs->bell = gnome_config_get_bool ("Options/SilenceBell=TRUE");
  prefs->swap_keys = gnome_config_get_bool ("Options/SwapDEL=FALSE");
  prefs->del_is_del = gnome_config_get_bool ("Options/DELisDEL=FALSE");

  prefs->transparent = gnome_config_get_bool ("Options/Transparent=TRUE");
  prefs->shaded = gnome_config_get_bool ("Options/Shaded=TRUE");

  temp = gnome_config_get_string ("Color/Palette=linux");
  for (i = 0; i < 4; i++) {
    if (!g_strcasecmp(temp, scheme[i])) {
      prefs->color_type = i;
    }
  }
  g_free (temp);

  prefs->color_set = gnome_config_get_int ("Color/Colors=0");

  temp = gnome_config_get_string ("Scroll/Position=right");
  for (i = 0; i < 3; i++) {
    if (!g_strcasecmp(temp, scpos[i])) {
      prefs->scrollbar_pos = i;
    }
  }
  g_free (temp);

  prefs->scroll_lines = gnome_config_get_int ("Scroll/History=2000");
  prefs->scroll_key = gnome_config_get_bool ("Scroll/Keypress=TRUE");
  prefs->scroll_out = gnome_config_get_bool ("Scroll/Output=FALSE");

  return prefs;
}

static void etherterm_idle_cb (gchar ** envp) {
  EtherWindow * window = NULL;

  window = g_new0 (EtherWindow, 1);

  window->num_pages = 0;
  window->window = gnome_app_new ("EtherTerm", "EtherTerm");

  gtk_widget_realize (window->window);
  gtk_signal_connect (GTK_OBJECT (window->window), "delete_event",
		      GTK_SIGNAL_FUNC (file_quit_callback), NULL);

  gtk_window_set_policy (GTK_WINDOW (window->window), FALSE, FALSE, FALSE);

  window->eprefs = et_prepare_app ();

  window->notebook = gtk_notebook_new ();
  gtk_notebook_set_scrollable (GTK_NOTEBOOK (window->notebook), TRUE);
  GTK_WIDGET_UNSET_FLAGS (window->notebook, GTK_CAN_FOCUS);
  gnome_app_set_contents (GNOME_APP (window->window), window->notebook);
  gtk_widget_show (window->notebook);

  et_create_menus (window);

  window->environ = envp;
  et_new_terminal_tab (window->window, window);
  gtk_widget_show (window->window);

  if (window->eprefs->menu_hide) {
    gtk_widget_hide (GNOME_APP (window->window)->menubar->parent);
  } else {
    gtk_widget_show (GNOME_APP (window->window)->menubar->parent);
  }
}

static struct poptOption popt_options[] = {
  { NULL, 0, 0, NULL, 0 }
};

extern char ** environ;

gint main (gint argc, gchar *argv[]) {
  poptContext ctx;
  const gchar ** args;

#ifdef ENABLE_NLS
  bindtextdomain (PACKAGE, GNOME_LOCALE_DIR);
  textdomain (PACKAGE);
#endif

  gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv,
			      popt_options, 0, &ctx);

  gdk_rgb_init ();

  args = poptGetArgs (ctx);
  if (args != NULL) {
    const gchar ** p;
    for (p = args; *p != NULL; p++) {
    }
  }
  if (environ != NULL) {
    gchar ** p;

    for (p = environ; *p != NULL; p++) {
      if (!g_strncasecmp (*p, "TERM=", strlen ("TERM="))) {
	*p = g_strdup ("TERM=xterm");
      }
    }
  }
  etherterm_idle_cb (environ);
  gtk_main ();

  return 0;
}
