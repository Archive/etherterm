#include "etherterm.h"

static gint et_scroll_get_pos (GtkOptionMenu * option_menu) {
  GtkWidget * widget;

  g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), -1);

  widget = gtk_menu_get_active (GTK_MENU (option_menu->menu));

  if (widget) {
    return g_list_index (GTK_MENU_SHELL (option_menu->menu)->children,
			 widget);
  } else {
    return -1;
  }
}

static void et_prefs_apply (GtkWidget * widget, gint page,
			    EtherWindow * window) {
  gchar * colors[18];
  gchar * scheme[4] = { "linux", "xterm", "rxvt", "custom" };
  gchar * scpos[3] = { "left", "right", "hidden" };
  gushort r, g, b;
  gint i;

  gnome_config_push_prefix ("EtherTerm/");

  window->eprefs->wordclass = g_strdup
    (gtk_entry_get_text (GTK_ENTRY (window->widgets->wordclass)));
  gnome_config_set_string ("Options/WordClass", window->eprefs->wordclass);

  window->eprefs->use_bold = GTK_TOGGLE_BUTTON (window->widgets->bold)->active;
  gnome_config_set_bool ("Options/Bold", window->eprefs->use_bold);

  window->eprefs->blink = GTK_TOGGLE_BUTTON (window->widgets->blink)->active;
  gnome_config_set_bool ("Options/Blink", window->eprefs->blink);

  window->eprefs->login = GTK_TOGGLE_BUTTON (window->widgets->login)->active;
  gnome_config_set_bool ("Options/Login", window->eprefs->login);

  window->eprefs->menu_hide =
    GTK_TOGGLE_BUTTON (window->widgets->menu_hide)->active;
  gnome_config_set_bool ("Options/HideMenu", window->eprefs->menu_hide);
  if (window->eprefs->menu_hide) {
    gtk_widget_hide (GNOME_APP (window->window)->menubar->parent);
  } else {
    gtk_widget_show (GNOME_APP (window->window)->menubar->parent);
  }

  window->eprefs->transparent =
    GTK_TOGGLE_BUTTON (window->widgets->bg_trans)->active;
  gnome_config_set_bool ("Options/Transparent", window->eprefs->transparent);

  window->eprefs->shaded =
    GTK_TOGGLE_BUTTON (window->widgets->bg_shaded)->active;
  gnome_config_set_bool ("Options/Shaded", window->eprefs->shaded);

  gnome_config_set_string ("Color/Palette",
			   scheme[window->eprefs->color_type]);
  for (i = 0; i < 16; i++) {
    gnome_color_picker_get_i16(GNOME_COLOR_PICKER(window->widgets->palette[i]),
			       &r, &g, &b, NULL);
    colors[i] = g_strdup_printf ("rgb:%04x/%04x/%04x",
				 r, g, b);
  }
  colors[16] = g_strdup ("rgb:bebe/bebe/bebe");
  colors[17] = g_strdup ("rgb:0000/0000/0000");
  gnome_config_set_vector ("Color/CustomPalette", 18, (const gchar **)colors);
  for (i = 0; i < 16; i++) {
    g_free (colors[i]);
  }

  window->eprefs->scrollbar_pos =
    et_scroll_get_pos (GTK_OPTION_MENU (window->widgets->scroll_pos));
  gnome_config_set_string ("Scroll/Position",
			   scpos[window->eprefs->scrollbar_pos]);

  gnome_config_sync ();
  gnome_config_pop_prefix ();
}

static void et_prop_changed (GtkWidget * widget, EtherWidgets * widgets) {
  if (GTK_TOGGLE_BUTTON (widgets->bg_none)->active ||
      GTK_TOGGLE_BUTTON (widgets->bg_trans)->active) {
    gtk_widget_set_sensitive (widgets->pixmap_file, FALSE);
    gtk_widget_set_sensitive (widgets->bg_scroll, FALSE);    
  } else {
    gtk_widget_set_sensitive (widgets->pixmap_file, TRUE);
    gtk_widget_set_sensitive (widgets->bg_scroll, TRUE);
  }
  gnome_property_box_changed (GNOME_PROPERTY_BOX (widgets->dialog));
}

static void et_color_changed (GtkWidget * w, gint r, gint g, gint b,
			      gint a, EtherWidgets * widgets) {
  et_prop_changed (w, widgets);
}

static void et_option_menu_changed (GtkMenuShell * menu_shell,
				    EtherWidgets * widgets) {
  et_prop_changed (NULL, widgets);
}

static void et_create_option_menu (GtkWidget * omenu, gchar ** menu_list,
				   EtherWindow * window) {
  GtkWidget * menu;
  int i = 0;

  menu = gtk_menu_new ();
  while (*menu_list) {
    GtkWidget * menu_item;

    menu_item = gtk_menu_item_new_with_label (*menu_list);
    gtk_menu_append (GTK_MENU (menu), menu_item);
    gtk_widget_show (menu_item);
    menu_list++;
    i++;
  }
  gtk_option_menu_set_menu (GTK_OPTION_MENU (omenu), menu);
  gtk_option_menu_set_history (GTK_OPTION_MENU (omenu),
			       window->eprefs->scrollbar_pos);
  gtk_widget_show (omenu);

  gtk_signal_connect (GTK_OBJECT (menu), "deactivate",
		      GTK_SIGNAL_FUNC (et_option_menu_changed),
		      window->widgets);
}

gchar * scroll_menus[] = {
  N_("Left"),
  N_("Right"),
  N_("Hidden"),
  NULL
};

void et_prefs_dialog (GtkWidget * parent, EtherWindow * window) {
  GtkWidget * page, * label, * box;
  EtherWidgets * widgets;
  GtkObject * spinadj;
  GSList * bg_group = NULL;

  window->widgets = widgets = g_new0 (EtherWidgets, 1);

  widgets->dialog = gnome_property_box_new ();
  widgets->notebook = GNOME_PROPERTY_BOX (widgets->dialog)->notebook;
  gtk_window_set_modal (GTK_WINDOW (widgets->dialog), TRUE);
  gtk_window_set_title (GTK_WINDOW (widgets->dialog),
			_("EtherTerm Properties"));
  gtk_window_set_policy (GTK_WINDOW (widgets->dialog), FALSE, FALSE, FALSE);

  /* General tab */
  page = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD_BIG);

  box = gtk_table_new (2, 3, FALSE);
  gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, 0);
  gtk_table_set_row_spacings (GTK_TABLE (box), 4);
  gtk_table_set_col_spacings (GTK_TABLE (box), 4);

  label = gtk_label_new (_("Font:"));
  gtk_table_attach (GTK_TABLE (box), label, 0, 1, 0, 1,
		    GTK_FILL, 0, 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label), 8, 0);
  gtk_widget_show (label);

  widgets->fontentry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->fontentry, 1, 2, 0, 1,
		    GTK_EXPAND | GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->fontentry);

  widgets->fontsel = gnome_font_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->fontsel, 2, 3, 0, 1,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->fontsel);

  label = gtk_label_new (_("Word Selection:"));
  gtk_table_attach (GTK_TABLE (box), label, 0, 1, 1, 2,
		    GTK_FILL, 0, 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label), 8, 0);
  gtk_widget_show (label);

  widgets->wordclass = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->wordclass, 1, 3, 1, 2,
		    GTK_EXPAND | GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->wordclass);
  gtk_entry_set_text (GTK_ENTRY (widgets->wordclass),
		      window->eprefs->wordclass);

  gtk_widget_show (box);

  box = gtk_table_new (2, 4, FALSE);
  gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (box), 4);
  gtk_table_set_row_spacings (GTK_TABLE (box), 4);

  widgets->bold = gtk_check_button_new_with_label (_("Enable Bold Text"));
  gtk_table_attach (GTK_TABLE (box), widgets->bold,0, 1, 0, 1,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->bold);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->bold),
				window->eprefs->use_bold);
  gtk_signal_connect (GTK_OBJECT (widgets->bold), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->blink = gtk_check_button_new_with_label (_("Blinking Cursor"));
  gtk_table_attach (GTK_TABLE (box), widgets->blink, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->blink);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->blink),
				window->eprefs->blink);
  gtk_signal_connect (GTK_OBJECT (widgets->blink), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->login = gtk_check_button_new_with_label (_("Use --login"));
  gtk_table_attach (GTK_TABLE (box), widgets->login, 2, 3, 0, 1,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->login);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->login),
				window->eprefs->login);
  gtk_signal_connect (GTK_OBJECT (widgets->login), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->menu_hide = gtk_check_button_new_with_label (_("Hide Menu Bar"));
  gtk_table_attach (GTK_TABLE (box), widgets->menu_hide, 3, 4, 0, 1,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->menu_hide);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->menu_hide),
				window->eprefs->menu_hide);
  gtk_signal_connect (GTK_OBJECT (widgets->menu_hide), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->bell = gtk_check_button_new_with_label (_("Silence Terminal Bell"));
  gtk_table_attach (GTK_TABLE (box), widgets->bell, 0, 1, 1, 2,
		   GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->bell);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->bell),
				window->eprefs->bell);
  gtk_signal_connect (GTK_OBJECT (widgets->bell), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->swapdel = gtk_check_button_new_with_label (_("Swap DEL/BS"));
  gtk_table_attach (GTK_TABLE (box), widgets->swapdel, 1, 2, 1, 2,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->swapdel);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->swapdel),
				window->eprefs->swap_keys);
  gtk_signal_connect (GTK_OBJECT (widgets->swapdel), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->delisdel = gtk_check_button_new_with_label (_("DEL is DEL/^H"));
  gtk_table_attach (GTK_TABLE (box), widgets->delisdel, 2, 3, 1, 2,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->delisdel);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->delisdel),
				window->eprefs->del_is_del);
  gtk_signal_connect (GTK_OBJECT (widgets->delisdel), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  gtk_widget_show (box);

  gtk_widget_show (page);
  gtk_notebook_append_page (GTK_NOTEBOOK (widgets->notebook), page,
			    gtk_label_new (_("General")));

  /* Image tab */
  page = gtk_table_new (6, 1, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (page), 4);

  widgets->bg_none = gtk_radio_button_new_with_label (bg_group, _("None"));
  bg_group = gtk_radio_button_group (GTK_RADIO_BUTTON (widgets->bg_none));
  gtk_table_attach (GTK_TABLE (page), widgets->bg_none, 0, 1, 0, 1,
		    GTK_FILL, 0, 0, 0);
  if (window->eprefs->transparent || window->eprefs->bg_pixmap) {
    GTK_TOGGLE_BUTTON (widgets->bg_none)->active = FALSE;
  }    
  gtk_widget_show (widgets->bg_none);
  gtk_signal_connect (GTK_OBJECT (widgets->bg_none), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->bg_pixmap = gtk_radio_button_new_with_label (bg_group,
							_("Pixmap"));
  bg_group = gtk_radio_button_group (GTK_RADIO_BUTTON (widgets->bg_pixmap));
  gtk_table_attach (GTK_TABLE (page), widgets->bg_pixmap, 0, 1, 1, 2,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->bg_pixmap);
  gtk_signal_connect (GTK_OBJECT (widgets->bg_pixmap), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  box = gtk_hbox_new (FALSE, 0);
  gtk_table_attach (GTK_TABLE (page), box, 0, 1, 2, 3,
		    GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

  label = gtk_label_new (_("Pixmap File:"));
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
  gtk_misc_set_padding (GTK_MISC (label), 8, 0);
  gtk_widget_show (label);

  widgets->pixmap_file = gnome_file_entry_new (NULL, NULL);
  gtk_box_pack_start (GTK_BOX (box), widgets->pixmap_file, TRUE, TRUE, 0);
  gtk_widget_show (widgets->pixmap_file);

  gtk_widget_show (box);

  widgets->bg_scroll = gtk_check_button_new_with_label (_("Pixmap Scrolls"));
  gtk_table_attach (GTK_TABLE (page), widgets->bg_scroll, 0, 1, 3, 4,
		    GTK_FILL, 0, 8, 0);
  gtk_widget_show (widgets->bg_scroll);
  gtk_signal_connect (GTK_OBJECT (widgets->bg_scroll), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  if (window->eprefs->transparent) {
    GTK_TOGGLE_BUTTON (widgets->bg_pixmap)->active = FALSE;
    gtk_widget_set_sensitive (widgets->pixmap_file, FALSE);
    gtk_widget_set_sensitive (widgets->bg_scroll, FALSE);
  }

  widgets->bg_trans = gtk_radio_button_new_with_label (bg_group,
						       _("Transparent"));
  bg_group = gtk_radio_button_group (GTK_RADIO_BUTTON (widgets->bg_trans));
  gtk_table_attach (GTK_TABLE (page), widgets->bg_trans, 0, 1, 4, 5,
		    GTK_FILL, 0, 0, 0);
  GTK_TOGGLE_BUTTON (widgets->bg_trans)->active = window->eprefs->transparent;
  gtk_widget_show (widgets->bg_trans);
  gtk_signal_connect (GTK_OBJECT (widgets->bg_trans), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  widgets->bg_shaded = gtk_check_button_new_with_label (_("Shaded"));
  gtk_table_attach (GTK_TABLE (page), widgets->bg_shaded, 0, 1, 5, 6,
		    GTK_FILL, 0, 0, 0);
  GTK_TOGGLE_BUTTON (widgets->bg_shaded)->active = window->eprefs->shaded;
  gtk_widget_show (widgets->bg_shaded);
  gtk_signal_connect (GTK_OBJECT (widgets->bg_shaded), "clicked",
		      GTK_SIGNAL_FUNC (et_prop_changed), widgets);

  gtk_widget_show (page);
  gtk_notebook_append_page (GTK_NOTEBOOK (widgets->notebook), page,
			    gtk_label_new (_("Background")));

  /* Colors tab */
  page = gtk_table_new (5, 2, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (page), 4);
  gtk_table_set_row_spacings (GTK_TABLE (page), 4);

  label = gtk_label_new (_("Color palette:"));
  gtk_table_attach (GTK_TABLE (page), label, 0, 1, 4, 5,
		    GTK_FILL, 0, 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label), 8, 0);
  gtk_widget_show (label);

  box = gtk_table_new (2, 8, FALSE);
  gtk_table_attach (GTK_TABLE (page), box, 1, 2, 4, 5,
		    GTK_FILL, GTK_FILL, 0, 0);

  widgets->palette[0] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[0],
		    0, 1, 0, 1, 0, 0, 0, 0);
  gtk_widget_show (widgets->palette[0]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[0]),
			      window->eprefs->palette[0].red,
			      window->eprefs->palette[0].green,
			      window->eprefs->palette[0].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[0]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[1] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[1],
		    1, 2, 0, 1, 0, 0, 0, 0);
  gtk_widget_show (widgets->palette[1]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[1]),
			      window->eprefs->palette[1].red,
			      window->eprefs->palette[1].green,
			      window->eprefs->palette[1].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[1]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[2] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[2], 2, 3, 0, 1,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[2]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[2]),
			      window->eprefs->palette[2].red,
			      window->eprefs->palette[2].green,
			      window->eprefs->palette[2].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[2]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[3] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[3], 3, 4, 0, 1,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[3]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[3]),
			      window->eprefs->palette[3].red,
			      window->eprefs->palette[3].green,
			      window->eprefs->palette[3].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[3]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[4] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[4], 4, 5, 0, 1,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[4]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[4]),
			      window->eprefs->palette[4].red,
			      window->eprefs->palette[4].green,
			      window->eprefs->palette[4].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[4]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[5] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[5], 5, 6, 0, 1,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[5]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[5]),
			      window->eprefs->palette[5].red,
			      window->eprefs->palette[5].green,
			      window->eprefs->palette[5].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[5]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[6] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[6], 6, 7, 0, 1,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[6]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[6]),
			      window->eprefs->palette[6].red,
			      window->eprefs->palette[6].green,
			      window->eprefs->palette[6].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[6]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[7] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[7], 7, 8, 0, 1,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[7]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[7]),
			      window->eprefs->palette[7].red,
			      window->eprefs->palette[7].green,
			      window->eprefs->palette[7].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[7]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[8] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[8], 0, 1, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[8]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[8]),
			      window->eprefs->palette[8].red,
			      window->eprefs->palette[8].green,
			      window->eprefs->palette[8].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[8]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[9] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[9], 1, 2, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[9]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[9]),
			      window->eprefs->palette[9].red,
			      window->eprefs->palette[9].green,
			      window->eprefs->palette[9].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[9]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[10] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[10], 2, 3, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[10]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[10]),
			      window->eprefs->palette[10].red,
			      window->eprefs->palette[10].green,
			      window->eprefs->palette[10].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[10]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[11] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[11], 3, 4, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[11]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[11]),
			      window->eprefs->palette[11].red,
			      window->eprefs->palette[11].green,
			      window->eprefs->palette[11].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[11]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[12] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[12], 4, 5, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[12]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[12]),
			      window->eprefs->palette[12].red,
			      window->eprefs->palette[12].green,
			      window->eprefs->palette[12].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[12]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[13] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[13], 5, 6, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[13]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[13]),
			      window->eprefs->palette[13].red,
			      window->eprefs->palette[13].green,
			      window->eprefs->palette[13].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[13]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[14] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[14], 6, 7, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[14]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[14]),
			      window->eprefs->palette[14].red,
			      window->eprefs->palette[14].green,
			      window->eprefs->palette[14].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[14]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  widgets->palette[15] = gnome_color_picker_new ();
  gtk_table_attach (GTK_TABLE (box), widgets->palette[15], 7, 8, 1, 2,
		    0, 0, 0, 0);
  gtk_widget_show (widgets->palette[15]);
  gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (widgets->palette[15]),
			      window->eprefs->palette[15].red,
			      window->eprefs->palette[15].green,
			      window->eprefs->palette[15].blue,
			      0);
  gtk_signal_connect (GTK_OBJECT (widgets->palette[15]), "color_set",
		      GTK_SIGNAL_FUNC (et_color_changed), widgets);

  gtk_widget_show (box);

  gtk_widget_show (page);
  gtk_notebook_append_page (GTK_NOTEBOOK (widgets->notebook), page,
			    gtk_label_new (_("Colors")));

  /* Scrolling tab */
  page = gtk_table_new (4, 2, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (page), 4);

  label = gtk_label_new (_("Scrollbar Position:"));
  gtk_table_attach (GTK_TABLE (page), label, 0, 1, 0, 1,
		    0, 0, 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label), 8, 0);
  gtk_widget_show (label);

  /* option menu here */
  widgets->scroll_pos = gtk_option_menu_new ();
  gtk_table_attach (GTK_TABLE (page), widgets->scroll_pos, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);
  et_create_option_menu (widgets->scroll_pos, scroll_menus, window);
  gtk_widget_show (widgets->scroll_pos);

  label = gtk_label_new (_("Scrollback Lines:"));
  gtk_table_attach (GTK_TABLE (page), label, 0, 1, 1, 2,
		    0, 0, 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5);
  gtk_misc_set_padding (GTK_MISC (label), 8, 0);
  gtk_widget_show (label);

  spinadj = gtk_adjustment_new (1, 1, 100000, 1, 5, 0);
  widgets->scroll_lines = gtk_spin_button_new (GTK_ADJUSTMENT (spinadj), 1, 0);
  gtk_table_attach (GTK_TABLE (page), widgets->scroll_lines, 1, 2, 1, 2,
		    GTK_FILL, 0, 0, 4);
  gtk_widget_show (widgets->scroll_lines);

  widgets->scroll_kbd = gtk_check_button_new_with_label
    (_("Scroll on Keypress"));
  gtk_table_attach (GTK_TABLE (page), widgets->scroll_kbd, 0, 2, 2, 3,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->scroll_kbd);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->scroll_kbd),
				window->eprefs->scroll_key);

  widgets->scroll_out = gtk_check_button_new_with_label
    (_("Scroll on Output"));
  gtk_table_attach (GTK_TABLE (page), widgets->scroll_out, 0, 2, 3, 4,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show (widgets->scroll_out);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widgets->scroll_out),
				window->eprefs->scroll_out);

  gtk_widget_show (page);
  gtk_notebook_append_page (GTK_NOTEBOOK (widgets->notebook), page,
			    gtk_label_new (_("Scrolling")));

  gtk_signal_connect (GTK_OBJECT (widgets->dialog), "apply",
		      GTK_SIGNAL_FUNC (et_prefs_apply), window);
  gtk_widget_show (widgets->dialog);
}
