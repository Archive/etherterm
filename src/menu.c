#include "etherterm.h"

void file_quit_callback (GtkWidget * widget, gpointer data) {
  gtk_main_quit ();
}

static void help_about_callback (GtkWidget *widget, void *data) {
  GtkWidget *about;
  const gchar *authors[] = {
    "Rodney Dawes <dobey@free.fr>",
    NULL
  };
  
  about = gnome_about_new (_("EtherTerm"),
			   VERSION,
			   /* copyright notice */
			   "Copyright (C) 2001 Elysium GNU/Linux",
			   authors,
			   /* other comments */
			   _("An easy to use tabbed terminal for GNOME"),
			   NULL);
  gtk_widget_show (about);
  
  return;
}

static GnomeUIInfo program_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("_New Terminal"), N_("Create a new terminal tab"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    FALSE, FALSE, NULL
  },
  { 
    GNOME_APP_UI_ITEM,
    N_("_Quit"), N_("Quit EtherTerm"),
    file_quit_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
    'Q', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("Tab Label"), N_("Edit the tab label"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK,
    FALSE, FALSE, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    N_("Preferences"), N_("Modify the terminal settings"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
    FALSE, FALSE, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo term_menu[] = {
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("About..."), N_("About EtherTerm"),
    help_about_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
    0, 0, NULL
  }, 
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[]= 
{
  GNOMEUIINFO_SUBTREE (N_("_Program"), program_menu),
  GNOMEUIINFO_SUBTREE (N_("_Edit"), edit_menu),
  GNOMEUIINFO_SUBTREE (N_("_Terminals"), term_menu),
  GNOMEUIINFO_SUBTREE (N_("_Help"), help_menu),
  GNOMEUIINFO_END
};

void et_edit_tab_label (GtkWidget * widget, EtherWindow * window) {
  GtkWidget * dialog, * entry;
  gchar * newtitle;

  dialog = gnome_dialog_new (_("Edit Tab Label"), GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL, NULL);
  gtk_widget_realize (dialog);
  entry = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER (GNOME_DIALOG (dialog)->vbox), entry);
  gtk_widget_show (entry);
  switch (gnome_dialog_run (GNOME_DIALOG (dialog))) {
  case 0: {
    newtitle = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
    gtk_notebook_set_tab_label_text (GTK_NOTEBOOK (window->notebook),
				     GTK_NOTEBOOK 
				     (window->notebook)->cur_page->child,
				     g_strdup (newtitle));
    g_free (newtitle);
    gnome_dialog_close (GNOME_DIALOG (dialog));
    break;
  }
  case 1: {
    gnome_dialog_close (GNOME_DIALOG (dialog));
    break;
  }
  default:
    break;
  }

};

void et_create_menus (EtherWindow * window) {
  gnome_app_create_menus (GNOME_APP (window->window), main_menu);
  gtk_signal_connect (GTK_OBJECT (program_menu[0].widget), "activate",
		      GTK_SIGNAL_FUNC (et_new_terminal_tab), window);
  gtk_signal_connect (GTK_OBJECT (edit_menu[0].widget), "activate",
		      GTK_SIGNAL_FUNC (et_edit_tab_label), window);
  gtk_signal_connect (GTK_OBJECT (edit_menu[1].widget), "activate",
		      GTK_SIGNAL_FUNC (et_prefs_dialog), window);
}
