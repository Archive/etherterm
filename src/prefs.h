#ifndef _ET_PREFS_H_
#define _ET_PREFS_H_

enum color_set_enum {
	COLORS_WHITE_ON_BLACK,
	COLORS_BLACK_ON_WHITE,
	COLORS_GREEN_ON_BLACK,
	COLORS_BLACK_ON_LIGHT_YELLOW,
	COLORS_CUSTOM
};

enum palette_enum {
	PALETTE_LINUX,
	PALETTE_XTERM,
	PALETTE_RXVT,
	PALETTE_CUSTOM
};

enum scrollbar_pos_enum {
	SCROLLBAR_LEFT   = 0,
	SCROLLBAR_RIGHT  = 1,
	SCROLLBAR_HIDDEN = 2
};

typedef struct {
  gchar * font;
  gchar * wordclass;
  gboolean use_bold;
  gboolean blink;
  gboolean login;
  gboolean menu_hide;
  gboolean bell;
  gboolean swap_keys;
  gboolean del_is_del;
  gint update_records;
  gboolean bg_pixmap;
  gboolean transparent;
  gboolean shaded;
  enum palette_enum color_type;
  enum color_set_enum color_set;
  GdkColor palette[18];
  enum scrollbar_pos_enum scrollbar_pos;
  gint scroll_lines;
  gboolean scroll_key;
  gboolean scroll_out;
} EtherPrefs;

typedef struct {
  GtkWidget * dialog;
  GtkWidget * notebook;
  GtkWidget * fontentry;
  GtkWidget * fontsel;
  GtkWidget * wordclass;
  GtkWidget * bold;
  GtkWidget * blink;
  GtkWidget * login;
  GtkWidget * menu_hide;
  GtkWidget * bell;
  GtkWidget * swapdel;
  GtkWidget * delisdel;
  GtkWidget * bg_none;
  GtkWidget * bg_pixmap;
  GtkWidget * pixmap_file;
  GtkWidget * bg_scroll;
  GtkWidget * bg_trans;
  GtkWidget * bg_shaded;
  GtkWidget * palette[18];
  GtkWidget * scroll_pos;
  GtkWidget * scroll_lines;
  GtkWidget * scroll_kbd;
  GtkWidget * scroll_out;
} EtherWidgets;

void et_prefs_dialog (GtkWidget * parent, EtherWindow * window);

#endif
