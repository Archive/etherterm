#ifndef _ET_MENU_H_
#define _ET_MENU_H_

void file_quit_callback (GtkWidget * widget, gpointer data);
void et_edit_tab_label (GtkWidget * widget, EtherWindow * window);
void et_create_menus (EtherWindow * window);

#endif
