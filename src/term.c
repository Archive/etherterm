#include "etherterm.h"

#define EXTRA 6

gushort linux_red[] = { 0x0000, 0xaaaa, 0x0000, 0xaaaa, 0x0000,
			0xaaaa, 0x0000, 0xaaaa,
			0x5555, 0xffff, 0x5555, 0xffff, 0x5555,
			0xffff,	0x5555, 0xffff,
			0x0,    0x0 };
gushort linux_grn[] = { 0x0000, 0x0000, 0xaaaa, 0x5555, 0x0000,
			0x0000, 0xaaaa, 0xaaaa,
			0x5555, 0x5555, 0xffff, 0xffff, 0x5555,
			0x5555, 0xffff, 0xffff,
			0x0,    0x0 };
gushort linux_blu[] = { 0x0000, 0x0000, 0x0000, 0x0000, 0xaaaa,
			0xaaaa, 0xaaaa, 0xaaaa,
			0x5555, 0x5555, 0x5555, 0x5555, 0xffff,
			0xffff, 0xffff, 0xffff,
			0x0,    0x0 };
gushort xterm_red[] = { 0x0000, 0x6767, 0x0000, 0x6767, 0x0000,
			0x6767, 0x0000, 0x6868,
			0x2a2a, 0xffff, 0x0000, 0xffff, 0x0000,
			0xffff, 0x0000, 0xffff,
			0x0,    0x0 };

gushort xterm_grn[] = { 0x0000, 0x0000, 0x6767, 0x6767, 0x0000,
			0x0000, 0x6767, 0x6868,
			0x2a2a, 0x0000, 0xffff, 0xffff, 0x0000,
			0x0000, 0xffff, 0xffff,
			0x0,    0x0 };
gushort xterm_blu[] = { 0x0000, 0x0000, 0x0000, 0x0000, 0x6767,
			0x6767, 0x6767, 0x6868,
			0x2a2a, 0x0000, 0x0000, 0x0000, 0xffff,
			0xffff, 0xffff, 0xffff,
			0x0,    0x0 };

gushort rxvt_red[] = { 0x0000, 0xffff, 0x0000, 0xffff, 0x0000,
		       0xffff, 0x0000, 0xffff,
		       0x0000, 0xffff, 0x0000, 0xffff, 0x0000,
		       0xffff, 0x0000, 0xffff,
		       0x0,    0x0 };
gushort rxvt_grn[] = { 0x0000, 0x0000, 0xffff, 0xffff, 0x0000,
		       0x0000, 0xffff, 0xffff,
		       0x0000, 0x0000, 0xffff, 0xffff, 0x0000,
		       0x0000, 0xffff, 0xffff,
		       0x0,    0x0 };
gushort rxvt_blu[] = { 0x0000, 0x0000, 0x0000, 0x0000, 0xffff,
		       0xffff, 0xffff, 0xffff,
		       0x0000, 0x0000, 0x0000, 0x0000, 0xffff,
		       0xffff, 0xffff, 0xffff,
		       0x0,    0x0 };

/* point to the other tables */
gushort *scheme_red[] = { linux_red, xterm_red, rxvt_red, rxvt_red };
gushort *scheme_blue[] = { linux_blu, xterm_blu, rxvt_blu, rxvt_blu };
gushort *scheme_green[] = { linux_grn, xterm_grn, rxvt_grn, rxvt_grn };

static void et_tab_title_changed (ZvtTerm * term, VTTITLE_TYPE type,
				  gchar * newtitle, EtherWindow * window) {
  /*
  gtk_window_set_title (GTK_WINDOW (window->window), g_strdup (newtitle));
  */
}

static void et_tab_child_died (ZvtTerm * term, EtherWindow * window) {
  gint page_num = gtk_notebook_page_num (GTK_NOTEBOOK (window->notebook),
					 GTK_WIDGET (term));

  if ((page_num <= 0) && (window->num_pages == 1)) {
    file_quit_callback (window->window, window);
  } else {
    gtk_notebook_remove_page (GTK_NOTEBOOK (window->notebook), page_num);
    window->num_pages--;
    if (window->num_pages == 1) {
      gtk_notebook_set_show_tabs (GTK_NOTEBOOK (window->notebook), FALSE);
    }
  }
}

static void et_get_shell_name (char **shell, char **name, gboolean isLogin) {
  char *only_name;
  int len;
  
  *shell = gnome_util_user_shell ();
  g_assert (*shell != NULL);
  
  only_name = strrchr (*shell, '/');
  if (only_name != NULL)
    only_name++;
  else
    only_name = *shell;
  
  if (isLogin){
    len = strlen (only_name);
    
    *name  = g_malloc (len + 2);
    **name = '-';
    strcpy ((*name)+1, only_name); 
  } else {
    *name = g_strdup (only_name);
  }
}

static void et_term_set_font (ZvtTerm * term, gchar * font_name,
				  const gint use_bold) {
  GdkFont * font;

  font = gdk_font_load (font_name);
  if (font) {
#ifdef ZVT_TERM_EMBOLDEN_SUPPORT
    if (zvt_term_get_capabilities (term) & ZVT_TERM_EMBOLDEN_SUPPORT &&
	use_bold) {
      zvt_term_set_fonts (term, font, 0);
    } else
#endif
      zvt_term_set_fonts (term, font, font);
  }
}

static void et_term_set_colors (GtkWidget * zterm, EtherWindow * window) {
  GdkColor c;
  gushort red[18], green[18], blue[18];
  gint i;
  gushort * r, * g, * b;

  gtk_widget_realize (zterm);
  switch (window->eprefs->color_type) {
  default:
    window->eprefs->color_type = 0;
  case PALETTE_LINUX:
  case PALETTE_XTERM:
  case PALETTE_RXVT: {
    r = scheme_red[window->eprefs->color_type];
    g = scheme_green[window->eprefs->color_type];
    b = scheme_blue[window->eprefs->color_type];
    for (i = 0; i < 18; i++) {
      red[i] = r[i];
      green[i] = g[i];
      blue[i] = b[i];
      window->eprefs->palette[i].red = r[i];
      window->eprefs->palette[i].blue = b[i];
      window->eprefs->palette[i].green = g[i];
    }
    break;
  }
  case PALETTE_CUSTOM: {
    for (i = 0; i < 18; i++) {
      red[i] = window->eprefs->palette[i].red;
      green[i] = window->eprefs->palette[i].green;
      blue[i] = window->eprefs->palette[i].blue;
    }
    break;
  }
  }

  switch (window->eprefs->color_set) {
  case COLORS_WHITE_ON_BLACK: {
    red   [16] = red [7];
    blue  [16] = blue [7];
    green [16] = green [7];
    red   [17] = red [0];
    blue  [17] = blue [0];
    green [17] = green [0];
    break;
  }
  case COLORS_BLACK_ON_WHITE: {
    red   [16] = red [0];
    blue  [16] = blue [0];
    green [16] = green [0];
    red   [17] = red [7];
    blue  [17] = blue [7];
    green [17] = green [7];
    break;
  }
  case COLORS_GREEN_ON_BLACK: {
    red   [17] = 0;
    green [17] = 0;
    blue  [17] = 0;
    red   [16] = 0;
    green [16] = 0xffff;
    blue  [16] = 0;
    break;
  }
  case COLORS_BLACK_ON_LIGHT_YELLOW: {
    red   [16] = 0;
    green [16] = 0;
    blue  [16] = 0;
    red   [17] = 0xffff;
    green [17] = 0xffff;
    blue  [17] = 0xdddd;
    break;
  }
  case COLORS_CUSTOM: {
    for (i = 16; i < 18; i++) {
      red[i] = window->eprefs->palette[i].red;
      green[i] = window->eprefs->palette[i].green;
      blue[i] = window->eprefs->palette[i].blue;
    }
    break;
  }
  }

  zvt_term_set_color_scheme (ZVT_TERM (zterm), red, green, blue);
  c.pixel = ZVT_TERM (zterm)->colors[17];
  gdk_window_set_background (zterm->window, &c);

  gtk_widget_queue_draw (zterm);
}

static void et_term_change_pos (GtkWidget * widget,
				GdkEventConfigure * event,
				GtkWidget * term) {
  gtk_widget_queue_draw (widget);
}

static GnomeUIInfo popup_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("_New Terminal"), N_("Create a new terminal tab"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    FALSE, FALSE, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    N_("Tab Label"), N_("Edit the tab label"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK,
    FALSE, FALSE, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM,
    N_("Preferences"), N_("Modify the terminal settings"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
    FALSE, FALSE, NULL
  },
  GNOMEUIINFO_TOGGLEITEM (N_("Hide Menu"), N_("Toggles Menu Hiding"),
			  NULL, NULL),
  GNOMEUIINFO_END
};

static void et_toggle_hide_menu (GtkWidget * widget, EtherWindow * window) {
  window->eprefs->menu_hide = GTK_CHECK_MENU_ITEM (widget)->active;

  if (window->eprefs->menu_hide) {
    gtk_widget_hide (GNOME_APP (window->window)->menubar->parent);
  } else {
    gtk_widget_show (GNOME_APP (window->window)->menubar->parent);
  }  
  gnome_config_push_prefix ("EtherTerm/");
  gnome_config_set_bool ("Options/HideMenu", window->eprefs->menu_hide);
  gnome_config_pop_prefix ();
}

static void et_term_popup_menu (GtkWidget * widget, GdkEventButton * event,
				EtherWindow * window) {
  GtkWidget * menu;

  if (event->button == 3) {
    gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "button_press_event");
    menu = gnome_popup_menu_new (popup_menu);

    gtk_signal_connect (GTK_OBJECT (popup_menu[0].widget), "activate",
			GTK_SIGNAL_FUNC (et_new_terminal_tab), window);
    gtk_signal_connect (GTK_OBJECT (popup_menu[1].widget), "activate",
			GTK_SIGNAL_FUNC (et_edit_tab_label), window);

    gtk_signal_connect (GTK_OBJECT (popup_menu[4].widget), "activate",
			GTK_SIGNAL_FUNC (et_prefs_dialog), window);

    GTK_CHECK_MENU_ITEM (popup_menu[5].widget)->active =
      window->eprefs->menu_hide;
    gtk_signal_connect (GTK_OBJECT (popup_menu[5].widget), "toggled",
			GTK_SIGNAL_FUNC (et_toggle_hide_menu), window);

    gnome_popup_menu_do_popup_modal (menu, NULL, NULL, event, window);
    gtk_widget_destroy (menu);
  }
}

void et_new_terminal_tab (GtkWidget * widget, EtherWindow * window) {
  GtkWidget * zterm, * hbox, * scrollbar;
  gchar * shell, * name;
  gint i = 0;

  zterm = zvt_term_new_with_size (80, 24);

  gtk_widget_show (zterm);
  gtk_widget_grab_focus (zterm);

  zvt_term_set_scrollback (ZVT_TERM (zterm), window->eprefs->scroll_lines);
  et_term_set_font (ZVT_TERM (zterm), window->eprefs->font,
		    window->eprefs->use_bold);
  zvt_term_set_wordclass (ZVT_TERM (zterm),
			  (guchar *)window->eprefs->wordclass);
  zvt_term_set_bell (ZVT_TERM (zterm), !window->eprefs->bell);
  zvt_term_set_blink (ZVT_TERM (zterm), window->eprefs->blink);

  zvt_term_set_scroll_on_output (ZVT_TERM (zterm),
				  window->eprefs->scroll_out);
  zvt_term_set_scroll_on_keystroke (ZVT_TERM (zterm),
				    window->eprefs->scroll_key);

  gtk_signal_connect (GTK_OBJECT (window->window), "configure_event",
		      GTK_SIGNAL_FUNC (et_term_change_pos), zterm);
  gtk_signal_connect (GTK_OBJECT (zterm), "child_died",
		      GTK_SIGNAL_FUNC (et_tab_child_died), window);
  gtk_signal_connect (GTK_OBJECT (zterm), "title_changed",
		      GTK_SIGNAL_FUNC (et_tab_title_changed), window);
  gtk_signal_connect (GTK_OBJECT (zterm), "button_press_event",
		      GTK_SIGNAL_FUNC (et_term_popup_menu), window);

#ifdef ZVT_TERM_MATCH_SUPPORT
  zvt_term_match_add( ZVT_TERM(zterm), "(((news|telnet|nttp|file|http|ftp|https)://)|(www|ftp))[-A-Za-z0-9\\.]+(:[0-9]*)?", VTATTR_UNDERLINE, "host only url");
  zvt_term_match_add( ZVT_TERM(zterm), "(((news|telnet|nttp|file|http|ftp|https)://)|(www|ftp))[-A-Za-z0-9\\.]+(:[0-9]*)?/[-A-Za-z0-9_\\$\\.\\+\\!\\*\\(\\),;:@&=\\?/~\\#\\%]*[^]'\\.}>\\) ,\\\"]", VTATTR_UNDERLINE, "full url");
#endif

  hbox = gtk_hbox_new (0, 0);
  gtk_widget_show (hbox);

  et_get_shell_name (&shell, &name, window->eprefs->login);

  scrollbar = gtk_vscrollbar_new (GTK_ADJUSTMENT(ZVT_TERM(zterm)->adjustment));
  GTK_WIDGET_UNSET_FLAGS (scrollbar, GTK_CAN_FOCUS);

  if (window->eprefs->scrollbar_pos == SCROLLBAR_LEFT) {
    gtk_box_pack_start (GTK_BOX (hbox), scrollbar, 0, 1, 0);
  } else {
    gtk_box_pack_end (GTK_BOX (hbox), scrollbar, 0, 1, 0);
  }
  if (window->eprefs->scrollbar_pos != SCROLLBAR_HIDDEN) {
    gtk_widget_show (scrollbar);
  }

  gtk_box_pack_start (GTK_BOX (hbox), zterm, 1, 1, 0);

  window->num_pages++;
  gtk_notebook_append_page (GTK_NOTEBOOK (window->notebook), hbox,
			    gtk_label_new (g_strdup (g_basename (shell))));
  if (window->num_pages == 1) {
    gtk_notebook_set_show_tabs (GTK_NOTEBOOK (window->notebook), FALSE);
  } else {
    gtk_notebook_set_show_tabs (GTK_NOTEBOOK (window->notebook), TRUE);
  }
  gtk_widget_show (zterm);

  if (window->eprefs->bg_pixmap) {
  } else if (window->eprefs->transparent) {
    zvt_term_set_background (ZVT_TERM (zterm), NULL,
			     window->eprefs->transparent,
			     window->eprefs->shaded);
  } else {
    zvt_term_set_background (ZVT_TERM (zterm), NULL, FALSE, FALSE);
  }

  et_term_set_colors (zterm, window);

  XSync (GDK_DISPLAY (), False);

  switch (zvt_term_forkpty (ZVT_TERM (zterm),
			    window->eprefs->update_records)) {
  case -1: {
    window->num_pages--;
    if (window->num_pages < 1) {
      gtk_main_quit ();
    } else {
      gint curpage = gtk_notebook_get_current_page (GTK_NOTEBOOK (window->notebook));
      gtk_notebook_remove_page (GTK_NOTEBOOK (window->notebook), curpage);
    }
    break;
  }
  case 0: {
    gint open_max = sysconf (_SC_OPEN_MAX);
    for (i = 3; i < open_max; i++) {
      fcntl (i, F_SETFD, 1);
    }
    execle (shell, name, NULL, window->environ);
  }
  }
  g_free (shell);
  g_free (name);

}
